﻿using CITEApp.Data.Interfaces;
using CITEApp.Dtos;
using CITEApp.Dtos.EmployeeWorkPositionHistoryDtos;
using CITEApp.Helpers.CustomExceptions;
using CITEApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data
{
    public class WorkPositionHistoryService : IWorkPositionHistoryService
    {
        private readonly DataContext _context;

        public WorkPositionHistoryService(DataContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<GetEmployeeWorkPositionHistoryDTO>> GetAllHistories(bool Active)
        {
            List<GetEmployeeWorkPositionHistoryDTO> employeeWorkPositionHistoryDTOs = new List<GetEmployeeWorkPositionHistoryDTO>();
            GetEmployeeWorkPositionHistoryDTO employeeWorkPositionHistoryDTO;
            List<EmployeeWorkingPositionHistory> employeeWorkingPositionHistories = new List<EmployeeWorkingPositionHistory>();
            if (Active)
                employeeWorkingPositionHistories = await _context.PositionHistories.Include("Position").Include("Employee").Where(i => i.Active).ToListAsync();
            else
                employeeWorkingPositionHistories = await _context.PositionHistories.Include("Position").Include("Employee").Where(i => !i.Active).ToListAsync();

            foreach (var history in employeeWorkingPositionHistories)
            {
                employeeWorkPositionHistoryDTO = new  GetEmployeeWorkPositionHistoryDTO
                {
                    Id = history.Id,
                    StartDate = history.StartDate,
                    StopDate=history.StopDate,
                    WorkPosition = new Dtos.WorkPositionDtos.GetWorkPositionVM
                    {
                        Id=history.Position.Id,
                        Description=history.Position.Description
                    },
                    Employee = new Dtos.EmployeeDtos.GetEmployeeVM
                    {
                        Id=history.Employee.Id,
                        Name=history.Employee.Name,
                        Surname=history.Employee.Surname,
                        DateStarted=history.Employee.DateStarted,
                        DateStopped=history.Employee.DateStopped
                    }
                };
                employeeWorkPositionHistoryDTOs.Add(employeeWorkPositionHistoryDTO);
            }
            return employeeWorkPositionHistoryDTOs;
        }

        public async Task<EmployeeWorkingPositionHistory> GetHistory(Guid id , bool trackResults)
        {
            EmployeeWorkingPositionHistory history;

            if (trackResults)
                history = await _context.PositionHistories.FirstOrDefaultAsync(i => i.Id == id);

            else
                history = await _context.PositionHistories.AsNoTracking().FirstOrDefaultAsync(i => i.Id == id);

            if (history == null)
                throw new BusinessLogicException($"There is no record with id: {id}");

            return history;
        }

        public async Task<EmployeeWorkingPositionHistory> CreateHistory(CreateEmployeeWorkingHistoryVM historyDTO)
        {
            EmployeeWorkingPositionHistory historyToCreate = new EmployeeWorkingPositionHistory
            {
                WorkPositionId = historyDTO.WorkPositionId,
                EmployeeId = historyDTO.EmployeeId,
                StartDate = DateTime.UtcNow
            };
            await _context.PositionHistories.AddAsync(historyToCreate);
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to create history");

            await _context.AddAsync(historyDTO);
            return historyToCreate;
        }
        public async Task<EmployeeWorkingPositionHistory> UpdateBranchHistory(UpdateEmployeeWorkingPositionHistoryVM historyDTO)
        {//TODO: useTransaction

            EmployeeWorkingPositionHistory HistoryToUpdate = await GetHistory(historyDTO.Id , true);

            if (!(historyDTO.WorkPositionId == null))
                HistoryToUpdate.WorkPositionId = historyDTO.WorkPositionId;

            if (!(historyDTO.EmployeeId == null))
                HistoryToUpdate.EmployeeId = historyDTO.EmployeeId;

            if (!(historyDTO.StartDate == null))
                HistoryToUpdate.StartDate = historyDTO.StartDate;

            if (!(historyDTO.StopDate == null))
                HistoryToUpdate.StopDate = historyDTO.StopDate;

            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to update branch");

            return HistoryToUpdate;
        }

        public async Task DeleteHistory(Guid id)
        {
            EmployeeWorkingPositionHistory historyToDelete = await GetHistory(id, true);
            historyToDelete.Active = false;
            _context.PositionHistories.Update(historyToDelete);

            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to delete history");
        }


        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

    }
}
