﻿using CITEApp._Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CITEApp.Helpers
{
    public class Util:IUtil
    {
        public string DisplayObjectInfo(Object o)
        {
            StringBuilder sb = new StringBuilder();

            Type type = o.GetType();
            sb.Append("Type: " + type.Name);
            sb.Append("Properties: ");
            PropertyInfo[] pi = type.GetProperties();
            if (pi.Length > 0)
            {
                foreach (PropertyInfo p in pi)
                {
                    Object val = p.GetValue(o, null);
                    List<SensitiveDataAttribute> isSensitive = p.GetCustomAttributes(typeof(SensitiveDataAttribute)).OfType<SensitiveDataAttribute>().ToList();
                    if (isSensitive.Count == 0)
                        sb.Append("  " + p.ToString() + " = " + val);

                    else
                    {
                        if (isSensitive.Any(i => i.Hidden))
                            continue;

                        sb.Append("  " + p.ToString() + " = " +  "*****");
                    }
                }
            }
            else
                sb.Append(" None");

            return sb.ToString();
        }
    }
}
