﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CITEApp.Data.Interfaces;
using CITEApp.Dtos;
using CITEApp.Dtos.EquipmentDtos;
using CITEApp.Dtos.EquipmentTypeDtos;
using CITEApp.Dtos.WorkPositionDtos;
using CITEApp.Helpers.CustomExceptions;
using CITEApp.Models;
using Microsoft.EntityFrameworkCore;

namespace CITEApp.Data
{
    public class EquipmentService : IEquipmentService
    {
        private readonly DataContext _context;

        public EquipmentService(DataContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<GetEquipmentDTO>> GetAllEquipments(bool Active)
        {
            List<GetEquipmentDTO> equipmentDTOs = new List<GetEquipmentDTO>();
            GetEquipmentDTO equipmentDTO;
            List<Equipment> equipments = new List<Equipment>();
            if (Active)
                equipments = await _context.Equipment.Include("Position").Include("EquipmentType").Where(i => i.Active).ToListAsync();
            else
                equipments = await _context.Equipment.Include("Position").Include("EquipmentType").Where(i => !i.Active).ToListAsync();

            foreach (var equipment in equipments)
            {
                equipmentDTO = new GetEquipmentDTO
                {
                    Id = equipment.Id,
                    Description = equipment.Description,
                    WorkPosition = new GetWorkPositionVM
                    {
                        Id = equipment.Position.Id,
                        Description = equipment.Position.Description
                    },
                    Type = new GetEquipmentTypeVM
                    {
                        Id=equipment.EquipmentType.Id,
                        Name=equipment.EquipmentType.Name
                    }
                    
                };
                equipmentDTOs.Add(equipmentDTO);
            }
            return equipmentDTOs; 
        }

        public async Task<Equipment> GetEquipment(Guid equipmentId , bool trackResults)
        {
            Equipment equipment;

            if (trackResults)
                equipment = await _context.Equipment.FirstOrDefaultAsync(i => i.Id == equipmentId);

            else
                equipment = await _context.Equipment.AsNoTracking().FirstOrDefaultAsync(i => i.Id == equipmentId);

            if (equipment == null)
                throw new BusinessLogicException($"There is no equipment with id: {equipmentId}");

            return equipment;
        }

        public async Task<Equipment> CreateEquipment(CreateEquipmentVM equipmentDTO)
        {
            Equipment equipmentToCreate = new Equipment
            {
                Description = equipmentDTO.Description,
                TypeId = equipmentDTO.TypeId
            };
            await _context.AddAsync(equipmentToCreate);
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to create equipment");

            return equipmentToCreate;
        }

        public async Task<Equipment> UpdateEquipment(UpdateEquipmentVM updateEquipmentDTO)
        {
            Equipment equipmentToUpdate = await GetEquipment(updateEquipmentDTO.Id, true);

                equipmentToUpdate.Description = updateEquipmentDTO.Description;
                equipmentToUpdate.TypeId = updateEquipmentDTO.TypeId;
                equipmentToUpdate.WorkPositionId = updateEquipmentDTO.WorkPositionId;

            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to update equipment");

            return equipmentToUpdate;
        }

        public async Task DeleteEquipment(Guid id)
        {
            Equipment equipment = await GetEquipment(id , true);
            //remove equipment from work position
            equipment.WorkPositionId = null;
            //make equipment inactive
            equipment.Active = false;
            _context.Equipment.Update(equipment);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
