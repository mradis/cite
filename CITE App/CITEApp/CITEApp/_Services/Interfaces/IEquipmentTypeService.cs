﻿using CITEApp.Dtos.EquipmentTypeDtos;
using CITEApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data.Interfaces
{
    public interface IEquipmentTypeService
    {
        Task<IEnumerable<GetEquipmentTypeVM>> GetTypes(bool Active);
        Task<EquipmentType> GetEquipmentType(Guid equipmentId , bool trackResults);
        Task<EquipmentType> CreateEquipmentType(CreateEquipmentTypeDTO equipmentType);
        Task<EquipmentType> UpdateEquipmentType(UpdateEquipmentTypeDTO updateEquipmentTypeDTO);
        Task DeleteEquipmentType(Guid id);
        Task<bool> SaveAll();
        Task<bool> EquipmentTypeExists(EquipmentType equipmentType);
    }
}
