﻿using CITEApp.Dtos;
using CITEApp.Dtos.EmployeeWorkPositionHistoryDtos;
using CITEApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data.Interfaces
{
    public interface IWorkPositionHistoryService
    {
        Task<IEnumerable<GetEmployeeWorkPositionHistoryDTO>> GetAllHistories(bool Active);
        Task<EmployeeWorkingPositionHistory> GetHistory(Guid id , bool trackResults);
        Task<EmployeeWorkingPositionHistory> CreateHistory(CreateEmployeeWorkingHistoryVM createEmployeeWorkingHistoryDTO);
        Task<EmployeeWorkingPositionHistory> UpdateBranchHistory(UpdateEmployeeWorkingPositionHistoryVM historyDTO);
        Task DeleteHistory(Guid id);
        Task<bool> SaveAll();
    }
}
