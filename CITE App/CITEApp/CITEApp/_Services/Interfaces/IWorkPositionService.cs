﻿using CITEApp.Dtos;
using CITEApp.Dtos.WorkPositionDtos;
using CITEApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data.Interfaces
{
    public interface IWorkPositionService
    {
        Task<IEnumerable<GetWorkPositionVM>> GetAllWorkPositions(bool Active);
        Task<WorkPosition> GetWorkPosition(Guid workPositionId , bool trackResults);
        Task<WorkPosition> CreateWorkPosition(CreateWorkPositionDTO workPositionDTO);
        Task<WorkPosition> UpdateWorkPosition(UpdateWorkPositionDTO workPositionDTO);
        Task DeleteWorkPosition(Guid id);
        Task<bool> SaveAll();
        Task AddEmployeeWorkPosition(Guid positionId, Guid employeeId);
        Task RemoveEmployeeWorkPosition(Guid positionId, Guid employeeId);
    }
}
