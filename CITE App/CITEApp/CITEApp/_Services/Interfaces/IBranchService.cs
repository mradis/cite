﻿using CITEApp.Controllers;
using CITEApp.Dtos.BranchDtos;
using CITEApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data.Interfaces
{
    public interface IBranchService
    {
        Task<IEnumerable<GetBranchDTO>> GetAllBranches(bool Active);
        Task<Branch> GetBranch(Guid branchId, bool trackResults);
        Task<Branch> CreateBranch(CreateBranchVM createBranchDTO);
        Task<Branch> UpdateBranch(UpdateBranchVM branch);
        Task DeleteBranch(Guid id);
        Task<bool> SaveAll();
        Task<bool> BranchExists(Branch branch);

    }
}
