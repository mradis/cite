﻿using CITEApp.Dtos;
using CITEApp.Dtos.EquipmentDtos;
using CITEApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data.Interfaces
{
    public interface IEquipmentService
    {
        Task<IEnumerable<GetEquipmentDTO>> GetAllEquipments(bool Active);
        Task<Equipment> GetEquipment(Guid equipmentId, bool trackResults);
        Task<Equipment> CreateEquipment(CreateEquipmentVM equipment);
        Task DeleteEquipment(Guid id);
        Task<Equipment> UpdateEquipment(UpdateEquipmentVM equipmentDTO);
        Task<bool> SaveAll();
    }
}
