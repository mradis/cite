﻿using CITEApp.Dtos;
using CITEApp.Dtos.EmployeeDtos;
using CITEApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data.Interfaces
{
    public interface IEmployeeService
    {
        Task<IEnumerable<GetEmployeeVM>> GetAllEmployees(bool Active);
        Task<Employee> GetEmployee(Guid employeeId , bool trackResults);
        Task<Employee> CreateEmployee(CreateEmployeeDTO employee);
        Task<Employee> UpdateEmployee(UpdateEmployeeDTO employee);
        Task DeleteEmployee(Guid id);
        Task<bool> SaveAll();
    }
}
