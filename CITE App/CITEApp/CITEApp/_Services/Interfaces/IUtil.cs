﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp._Services.Interfaces
{
    public interface IUtil
    {
        string DisplayObjectInfo(Object o);
    }
}
