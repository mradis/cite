﻿using CITEApp.Data.Interfaces;
using CITEApp.Dtos.EquipmentTypeDtos;
using CITEApp.Helpers.CustomExceptions;
using CITEApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data
{
    public class EquipmentTypeService : IEquipmentTypeService
    {
        private readonly DataContext _context;

        public EquipmentTypeService(DataContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<GetEquipmentTypeVM>> GetTypes(bool Active)
        {
            List<GetEquipmentTypeVM> equipmentTypeDTOs = new List<GetEquipmentTypeVM>();
            GetEquipmentTypeVM equipmentTypeDTO;
            List<EquipmentType> equipmentTypes = new List<EquipmentType>();
            if (Active)
                equipmentTypes = await _context.EquipmentTypes.Where(i => i.Active).ToListAsync();
            else
                equipmentTypes = await _context.EquipmentTypes.Where(i => !i.Active).ToListAsync();

            foreach (var equipmentType in equipmentTypes)
            {
                equipmentTypeDTO = new GetEquipmentTypeVM
                {
                    Id = equipmentType.Id,
                    Name =equipmentType.Name
                };
                equipmentTypeDTOs.Add(equipmentTypeDTO);
            }
            return equipmentTypeDTOs;
        }
        public async Task<EquipmentType> GetEquipmentType(Guid equipmentId , bool trackResults)
        {
            EquipmentType equipmentType;

            if (trackResults)
                equipmentType = await _context.EquipmentTypes.FirstOrDefaultAsync(i => i.Id == equipmentId);

            else
                equipmentType = await _context.EquipmentTypes.AsNoTracking().FirstOrDefaultAsync(i => i.Id == equipmentId);

            if (equipmentType == null)
                throw new BusinessLogicException($"There is no equipment type with id: {equipmentId}");

            return equipmentType;
        }

        public async Task<EquipmentType> CreateEquipmentType(CreateEquipmentTypeDTO equipmentType)
        {//TODO: useTransaction
            EquipmentType equipmentTypeToCreate = new EquipmentType
            {
                Name = equipmentType.Name
            };
            // Check if type already exists
            if ((await EquipmentTypeExists(equipmentTypeToCreate)))
                throw new BusinessLogicException("This equipment type already exists");

            await _context.AddAsync(equipmentTypeToCreate);
            await SaveAll();
            return equipmentTypeToCreate;
        }

        public async Task<EquipmentType> UpdateEquipmentType(UpdateEquipmentTypeDTO updateEquipmentTypeDTO)
        {
            EquipmentType equipmentTypeToUpdate = new EquipmentType { Name = updateEquipmentTypeDTO.Name };
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to update equipmentType");

            return equipmentTypeToUpdate;
        }


        public async Task DeleteEquipmentType(Guid Id)
        {//TODO: useTransaction
            EquipmentType equipmentType = await GetEquipmentType(Id , true);
            //Check if type has registered equipments
            int typeEquipmentCounts = await TypeHasEquipment(equipmentType);
            if (typeEquipmentCounts > 0)
                throw new BusinessLogicException($"This equipment type type has {typeEquipmentCounts} registries first delete all the equipment registered to this type in order to delete the type");

            equipmentType.Active = false;
            _context.EquipmentTypes.Update(equipmentType);

            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to delete equipmentType");
        }

        public async Task<int> TypeHasEquipment(EquipmentType equipmentType)
        {
            return await _context.Equipment.Where(i => i.EquipmentType.Id == equipmentType.Id).CountAsync();
        }


        public async Task<bool> EquipmentTypeExists(EquipmentType equipmentType)
        {
            if ((await _context.EquipmentTypes.FirstOrDefaultAsync(i => i.Name == equipmentType.Name) == null))
                return false;

            return true;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
