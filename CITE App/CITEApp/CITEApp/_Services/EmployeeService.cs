﻿using CITEApp.Data.Interfaces;
using CITEApp.Dtos;
using CITEApp.Dtos.EmployeeDtos;
using CITEApp.Helpers.CustomExceptions;
using CITEApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data
{
    public class EmployeeService : IEmployeeService
    {
        private readonly DataContext _context;
        public EmployeeService(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GetEmployeeVM>> GetAllEmployees(bool Active)
        {
            List<GetEmployeeVM> employeeDTOs = new List<GetEmployeeVM>();
            GetEmployeeVM employeeDTO;
            List<Employee> employees = new List<Employee>();
            if (Active)
                employees = await _context.Employees.Where(i => i.Active).ToListAsync();
            else
                employees = await _context.Employees.Where(i => !i.Active).ToListAsync();

            foreach (var employee in employees)
            {
                employeeDTO = new GetEmployeeVM
                {
                    Id=employee.Id,
                    Name=employee.Name,
                    Surname=employee.Surname,
                    DateStarted=employee.DateStarted,
                    DateStopped=employee.DateStopped
                };
                employeeDTOs.Add(employeeDTO);
            }
            return employeeDTOs;
        }

        public async Task<Employee> GetEmployee(Guid employeeId ,bool trackResults)
        {
            Employee employee;

            if (trackResults)
                employee = await _context.Employees.FirstOrDefaultAsync(i => i.Id == employeeId);

            else
                employee = await _context.Employees.AsNoTracking().FirstOrDefaultAsync(i => i.Id == employeeId);

            if (employee == null)
                throw new BusinessLogicException($"There is no Employee with id: {employeeId}");

            return employee;
        }

        public async Task<Employee> CreateEmployee(CreateEmployeeDTO employee)
        {
            Employee employeeToCreate = new Employee
            {
                Name = employee.Name,
                Surname = employee.Surname
            };
            await _context.Employees.AddAsync(employeeToCreate);
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to create employee");

            return employeeToCreate;
        }
        public async Task<Employee> UpdateEmployee(UpdateEmployeeDTO employee)
        {
            Employee employeeToUpdate = await GetEmployee(employee.Id ,true);
            employeeToUpdate.Name = employee.Name;
            employeeToUpdate.Surname = employee.Surname;
            employeeToUpdate.Salary = employee.Salary;

            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to update employee");

            return employeeToUpdate;
        }

        public async Task DeleteEmployee(Guid id)
        {
            Employee employee = await GetEmployee(id , true);
            WorkPosition position = await _context.WorkPositions.FirstOrDefaultAsync(i => i.EmployeeId == employee.Id);
            // Remove employee id from work position
            //TODO: update work position history
            if (position != null)
            {
                position.EmployeeId = null;
                _context.WorkPositions.Update(position);
            }
            employee.Active = false;
            _context.Employees.Update(employee);
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to delete user");
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

    }
}
