﻿using CITEApp.Data.Interfaces;
using CITEApp.Dtos;
using CITEApp.Dtos.BranchDtos;
using CITEApp.Dtos.EmployeeDtos;
using CITEApp.Dtos.WorkPositionDtos;
using CITEApp.Helpers.CustomExceptions;
using CITEApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data
{
    public class WorkPositionService : IWorkPositionService
    {
        private readonly DataContext _context;

        public WorkPositionService(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GetWorkPositionVM>> GetAllWorkPositions(bool Active)
        {
            List<GetWorkPositionVM> workPositionDTOs = new List<GetWorkPositionVM>();
            GetWorkPositionVM workPositionDTO;
            List<WorkPosition> workPositions = new List<WorkPosition>();
            if (Active)
                workPositions = await _context.WorkPositions.Include("Branch").Include("Employee").Where(i => i.Active).ToListAsync();
            else
                workPositions = await _context.WorkPositions.Include("Branch").Include("Employee").Where(i => !i.Active).ToListAsync();

            foreach (var workPosition in workPositions)
            {
                if (workPosition.Employee == null)
                {
                    workPositionDTO = new GetWorkPositionVM
                    {
                        Id = workPosition.Id,
                        Description = workPosition.Description,
                        Branch = new GetBranchDTO
                        {
                            Id = workPosition.Branch.Id,
                            Name = workPosition.Branch.Name,
                            Address = workPosition.Branch.Address,
                            Created = workPosition.Branch.Created
                        }
                    };
                }
                else
                {
                    workPositionDTO = new GetWorkPositionVM
                    {
                        Id = workPosition.Id,
                        Description = workPosition.Description,
                        Branch = new GetBranchDTO
                        {
                            Id = workPosition.Branch.Id,
                            Name = workPosition.Branch.Name,
                            Address = workPosition.Branch.Address,
                            Created = workPosition.Branch.Created
                        },
                        Employee = new GetEmployeeVM
                        {
                            Id = workPosition.Employee.Id,
                            Name = workPosition.Employee.Name,
                            Surname = workPosition.Employee.Surname,
                            DateStarted = workPosition.Employee.DateStarted,
                            DateStopped = workPosition.Employee.DateStopped
                        }

                    };
                }

                workPositionDTOs.Add(workPositionDTO);
            }
            return workPositionDTOs;
        }

        public async Task<WorkPosition> GetWorkPosition(Guid workPositionId , bool trackResults)
        {
            WorkPosition position;

            if (trackResults)
                position = await _context.WorkPositions.FirstOrDefaultAsync(i => i.Id == workPositionId);

            else
                position = await _context.WorkPositions.AsNoTracking().FirstOrDefaultAsync(i => i.Id == workPositionId);

            if (position == null)
                throw new BusinessLogicException($"There is no work position with id: {workPositionId}");

            return position;
        }

        public async Task<WorkPosition> CreateWorkPosition(CreateWorkPositionDTO workPositionDTO)
        {
            WorkPosition positionToCreate = new WorkPosition()
            {
                BranchId = workPositionDTO.BranchId
            };
            await _context.AddAsync(positionToCreate);
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to create work position");

            return positionToCreate;
        }
        public async Task<WorkPosition> UpdateWorkPosition(UpdateWorkPositionDTO workPositionDTO)
        {
            WorkPosition workPositionToUpdate = await GetWorkPosition(workPositionDTO.Id , true);
            workPositionToUpdate.EmployeeId = workPositionDTO.EmployeeId;
            workPositionToUpdate.BranchId = workPositionDTO.BranchId;

            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to create work position");

            return workPositionToUpdate;
        }

        public async Task DeleteWorkPosition(Guid id)
        {//TODO: useTransaction
            WorkPosition positionToDelete = await GetWorkPosition(id,true);
            if (positionToDelete.Employee != null)
                throw new BusinessLogicException($"You can't delete this work position first remove {positionToDelete.Employee.Name} {positionToDelete.Employee.Surname} from position {positionToDelete.Id} and then try again");

            positionToDelete.Active = false;
            _context.WorkPositions.Update(positionToDelete);
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to delete working position");
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task AddEmployeeWorkPosition(Guid positionId, Guid employeeId)
        {//TODO: useTransaction
            WorkPosition position = await GetWorkPosition(positionId, true);
            if (position == null)
                throw new BusinessLogicException("This work position does not exist");

            if (_context.Employees.FirstOrDefault(i => i.Id == employeeId) == null)
                throw new BusinessLogicException("This user does not exist");

            if (position.EmployeeId != null)
                throw new BusinessLogicException($"This position is registered to {position.Employee.Name}");

            position.EmployeeId = employeeId;

            //Create a new entry at work position history
            EmployeeWorkingPositionHistory positionHistory = new EmployeeWorkingPositionHistory()
            {
                WorkPositionId = positionId,
                EmployeeId = employeeId,
                StartDate = DateTime.UtcNow
            };

            await _context.PositionHistories.AddAsync(positionHistory);

            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to update employee");
        }

        public async Task RemoveEmployeeWorkPosition(Guid positionId, Guid employeeId)
        {//TODO: useTransaction
            WorkPosition position = await GetWorkPosition(positionId, true);
            if (position == null)
                throw new BusinessLogicException("This work position does not exist");

            if (_context.Employees.FirstOrDefault(i => i.Id == employeeId) == null)
                throw new BusinessLogicException("This user does not exist");

            if (position.Employee.Id != employeeId)
                throw new BusinessLogicException($"This position is registered to {position.Employee.Name}");

            if (position.EmployeeId == null)
                throw new BusinessLogicException($"This position is already empty");

            //TODO:Update work position history
            EmployeeWorkingPositionHistory positionHistory = await _context.PositionHistories.FirstOrDefaultAsync(i => i.WorkPositionId == positionId && i.EmployeeId == employeeId);
            positionHistory.StopDate = DateTime.UtcNow;

            position.EmployeeId = null;
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to update employee");
        }

    }
}
