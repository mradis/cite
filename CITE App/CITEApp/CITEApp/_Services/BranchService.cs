﻿using CITEApp.Controllers;
using CITEApp.Data.Interfaces;
using CITEApp.Dtos.BranchDtos;
using CITEApp.Helpers.CustomExceptions;
using CITEApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Data
{
    public class BranchService : IBranchService 
    {
        private readonly DataContext _context;

        public BranchService(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GetBranchDTO>> GetAllBranches(bool Active)
        {
            List<GetBranchDTO> branchDTOs = new List<GetBranchDTO>();
            GetBranchDTO branchDTO;
            List<Branch> branches = new List<Branch>();
            if (Active)
                branches = await _context.Branches.Where(i => i.Active).ToListAsync();
            else
                branches = await _context.Branches.Where(i => !i.Active).ToListAsync();

            foreach (var branch in branches)
            {
                branchDTO = new GetBranchDTO
                {
                    Name = branch.Name,
                    Id = branch.Id,
                    Address = branch.Address,
                    Created = branch.Created
                };
                branchDTOs.Add(branchDTO);
            }
            return branchDTOs;
        }

        public async Task<Branch> GetBranch(Guid branchId , bool trackResults)
        {
            Branch branch;

            if (trackResults)
                branch = await _context.Branches.FirstOrDefaultAsync(i => i.Id == branchId);

            else
                branch = await _context.Branches.AsNoTracking().FirstOrDefaultAsync(i => i.Id == branchId);

            if (branch == null)
                throw new BusinessLogicException($"There is no Branch with id: {branchId}");

                return branch;
        }

        public async Task<Branch> CreateBranch(CreateBranchVM createBranchDTO)
        {//TODO: useTransaction  i used transaction here
            Branch branchToCreate=new Branch();
            using (var transaction =_context.Database.BeginTransaction())
            {
                try
                {
                    branchToCreate = new Branch
                    {
                        Name = createBranchDTO.Name,
                        Address = createBranchDTO.Address,
                        Created = DateTime.UtcNow
                    };
                    if (await BranchExists(branchToCreate))
                        throw new BusinessLogicException("Branch already exists");

                    await _context.AddAsync(branchToCreate);
                    if (!(await SaveAll()))
                        throw new BusinessLogicException("Failed to create branch");

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }

            }
            return branchToCreate;
        }
        public async Task<Branch> UpdateBranch(UpdateBranchVM branch)
        {

            Branch BranchToUpdate = await GetBranch(branch.Id ,true);
            BranchToUpdate.Name = branch.Name;
            BranchToUpdate.Address = branch.Address;

            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to update branch");

            return BranchToUpdate;
        }

        public async Task DeleteBranch(Guid id)
        {
            //TODO: useTransaction
            Branch branchToDelete = await GetBranch(id, true);
            int positionCounts = await BranchHasPositions(branchToDelete);
            //checking if the branch has active positions
            if (positionCounts > 0)
            {
                if (positionCounts == 1)
                {
                    throw new BusinessLogicException($"This branch has one work position registry first delete all the positions registered to this branch in order to delete the branch");
                }
                throw new BusinessLogicException($"This branch has {positionCounts} work positions registries first delete all the positions registered to this branch in order to delete the branch");
            }

            branchToDelete.Active = false;
            _context.Branches.Update(branchToDelete);
            if (!(await SaveAll()))
                throw new FailedToSaveDataException("Failed to delete user");
        }
        public async Task<int> BranchHasPositions(Branch branch)
        {
            return await _context.WorkPositions.Where(i => i.Branch.Id == branch.Id).CountAsync();
        }
        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> BranchExists(Branch branch)
        {
            if ((await _context.Branches.FirstOrDefaultAsync(i => i.Name == branch.Name) == null) || (await _context.Branches.FirstOrDefaultAsync(i => i.Address == branch.Address) == null))
                return false;

            return true;
        }



    }
}
