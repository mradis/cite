﻿using CITEApp.Data;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Helpers.CustomFilters
{
    public class TransactionActionFilter : IAsyncActionFilter
    {
        private readonly DataContext _context;
        private readonly ILogger _logger;

        public TransactionActionFilter(DataContext context, ILogger<TransactionActionFilter> logger)
        {
            _context = context;
            _logger = logger;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _logger.LogDebug("transaction began");
                    ActionExecutedContext nextResult = await next();
                    if (nextResult.Exception == null)
                    {
                        transaction.Commit();
                        _logger.LogDebug("transaction commited");
                    }
                    else
                    {
                        _context.Database.RollbackTransaction();
                        _logger.LogDebug("transaction Rolled Back");
                        throw nextResult.Exception;
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }

    }
}
