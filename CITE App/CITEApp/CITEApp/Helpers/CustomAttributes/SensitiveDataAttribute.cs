﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SensitiveDataAttribute : Attribute
    {
        public bool Hidden { get; private set; }


        public SensitiveDataAttribute(bool hidden = true)
        {
            this.Hidden = hidden;
        }
    }
}
