﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Helpers.CustomExceptions
{
    [Serializable]
    public class FailedToSaveDataException:Exception
    {
        public FailedToSaveDataException()
        {

        }
        public FailedToSaveDataException(string message) : base(String.Format(message))
        {

        }
    }
}
