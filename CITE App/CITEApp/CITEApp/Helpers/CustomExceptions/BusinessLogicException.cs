﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Helpers.CustomExceptions
{
    public class BusinessLogicException:Exception
    {
        public BusinessLogicException()
        {

        }
        public BusinessLogicException(string message):base(String.Format(message))
        {

        }
    }
}
