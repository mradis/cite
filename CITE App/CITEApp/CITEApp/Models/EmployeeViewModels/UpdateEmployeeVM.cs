﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos
{
    public class UpdateEmployeeDTO
    {
        [Required(ErrorMessage = "UpdateEmployeeIdRequiredMessage")]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "UpdateEmployeeNameRequiredMessage")]
        public string Name { get; set; }
        [Required(ErrorMessage = "UpdateEmployeeSurnameRequiredMessage")]
        public string Surname { get; set; }
        public decimal Salary { get; set; } = 0;
    }
}
