﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos.EmployeeDtos
{
    public class GetEmployeeVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateStarted { get; set; }
        public DateTime? DateStopped { get; set; }
    }
}
