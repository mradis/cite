﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CITEApp.Dtos
{
    public class CreateEmployeeDTO
    {
        [Required(ErrorMessage = "CreateEmployeeNameRequiredMessage")]
        public string Name { get; set; }
        [Required(ErrorMessage = "CreateEmployeeSurnameRequiredMessage")]
        public string Surname { get; set; }

        //for testing the Custom attribute
        [SensitiveData(false)]
        public string SensitiveInfo { get; set; }
        [SensitiveData()]
        public string VerySensitiveInfo { get; set; }
        public string ClainInfo { get; set; }
    }
}
