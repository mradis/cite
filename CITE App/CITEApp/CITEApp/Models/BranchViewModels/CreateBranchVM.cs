﻿using System.ComponentModel.DataAnnotations;

namespace CITEApp.Controllers
{
    public class CreateBranchVM
    {
         
        [Required(ErrorMessage = "CreateBranchNameRequiredMessage")]
        public string Name { get; set; }
        [Required(ErrorMessage = "CreateBranchAddressRequiredMessage")]
        public string Address { get; set; }
    }
}