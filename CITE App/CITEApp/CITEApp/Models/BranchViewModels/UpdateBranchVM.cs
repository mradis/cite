﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CITEApp.Controllers
{
    public class UpdateBranchVM
    {
        [Required(ErrorMessage = "UpdateBranchIdRequiredMessage")]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "UpdateBranchNameRequiredMessage")]
        public string Name { get; set; }
        [Required(ErrorMessage = "UpdateBranchAddressRequiredMessage")]
        public string Address { get; set; }
        public DateTime Created { get; set; }


    }
}