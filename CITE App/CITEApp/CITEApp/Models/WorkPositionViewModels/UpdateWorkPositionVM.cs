﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos
{
    public class UpdateWorkPositionDTO
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public Guid BranchId { get; set; }
        public Guid EmployeeId { get; set; }
    }
}
