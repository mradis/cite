﻿using CITEApp.Dtos.BranchDtos;
using CITEApp.Dtos.EmployeeDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos.WorkPositionDtos
{
    public class GetWorkPositionVM
    {
        public Guid Id { get; set; }
        public GetBranchDTO Branch { get; set; }
        public GetEmployeeVM Employee { get; set; }
        public string Description { get; set; }
    }
}
