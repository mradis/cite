﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos
{
    public class CreateWorkPositionDTO
    {
        [Required]
        public Guid BranchId { get; set; }
    }
}
