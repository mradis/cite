﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos.EquipmentTypeDtos
{
    public class CreateEquipmentTypeDTO
    {
        [Required(ErrorMessage = "CreateEquipmentTypeNameRequiredMessage")]
        public string Name { get; set; }
    }
}
