﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos.EquipmentTypeDtos
{
    public class GetEquipmentTypeVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
