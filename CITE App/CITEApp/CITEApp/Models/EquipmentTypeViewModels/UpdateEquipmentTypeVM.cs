﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos.EquipmentTypeDtos
{
    public class UpdateEquipmentTypeDTO
    {
        [Required(ErrorMessage = "UpdateEquipmentTypeIdRequiredMessage")]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "UpdateEquipmentTypeNameRequiredMessage")]
        public string Name { get; set; }
    }
}
