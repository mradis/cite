﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos
{
    public class CreateEmployeeWorkingHistoryVM
    {
        [Required(ErrorMessage = "CreateEmployeeWorkingHistoryWorkPositionIdRequiredMessage")]
        public Guid WorkPositionId { get; set; }
        [Required(ErrorMessage = "CreateEmployeeWorkingHistoryEmployeeIdRequiredMessage")]
        public Guid EmployeeId { get; set; }
        [Required(ErrorMessage = "CreateEmployeeWorkingHistoryStartDateRequiredMessage")]
        public DateTime StartDate { get; set; }

    }
}
