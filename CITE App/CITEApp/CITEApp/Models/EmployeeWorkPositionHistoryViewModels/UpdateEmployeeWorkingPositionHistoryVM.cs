﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos
{
    public class UpdateEmployeeWorkingPositionHistoryVM
    {
        [Required(ErrorMessage = "UpdateEmployeeWorkingPositionHistoryIdRequiredMessage")]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "UpdateEmployeeWorkingPositionHistoryWorkPositionIdRequiredMessage")]
        public Guid WorkPositionId { get; set; }
        [Required(ErrorMessage = "UpdateEmployeeWorkingPositionHistoryEmployeeIdRequiredMessage")]
        public Guid EmployeeId { get; set; }
        [Required(ErrorMessage = "UpdateEmployeeWorkingPositionHistoryEmployeeStartDateRequiredMessage")]
        public DateTime StartDate { get; set; }
      //  [Required(ErrorMessage = "UpdateEmployeeWorkingPositionHistoryStopDateRequiredMessage")]
        public DateTime? StopDate { get; set; }
    }
}
