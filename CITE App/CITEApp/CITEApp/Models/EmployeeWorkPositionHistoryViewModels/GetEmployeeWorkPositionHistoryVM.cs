﻿using CITEApp.Dtos.EmployeeDtos;
using CITEApp.Dtos.WorkPositionDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos.EmployeeWorkPositionHistoryDtos
{
    public class GetEmployeeWorkPositionHistoryDTO
    {
        public Guid Id { get; set; }
        public GetWorkPositionVM WorkPosition { get; set; }
        public GetEmployeeVM Employee { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? StopDate { get; set; }
    }
}
