﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos
{
    public class UpdateEquipmentVM
    {
        [Required(ErrorMessage = "UpdateEquipmentIdRequiredMessage")]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "UpdateEquipmentDescriptionRequiredMessage")]
        public string Description { get; set; }
        [Required(ErrorMessage = "UpdateEquipmentTypeIdRequiredMessage")]
        public Guid TypeId { get; set; }
        [Required(ErrorMessage = "UpdateEquipmentWorkPositionIdRequiredMessage")]
        public Guid? WorkPositionId { get; set; }
    }
}
