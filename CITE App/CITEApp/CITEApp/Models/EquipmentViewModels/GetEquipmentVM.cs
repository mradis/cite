﻿using CITEApp.Dtos.EquipmentTypeDtos;
using CITEApp.Dtos.WorkPositionDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos.EquipmentDtos
{
    public class GetEquipmentDTO
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public GetEquipmentTypeVM Type { get; set; }
        public GetWorkPositionVM WorkPosition { get; set; }
    }
}
