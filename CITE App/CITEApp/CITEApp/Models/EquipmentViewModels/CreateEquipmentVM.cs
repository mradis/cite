﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Dtos
{
    public class CreateEquipmentVM
    {
        [Required(ErrorMessage = "CreateEquipmentDescriptionRequiredMessage")]
        public string Description { get; set; }
        [Required(ErrorMessage = "CreateEquipmentTypeIdRequiredMessage")]
        public Guid TypeId { get; set; }
        public Guid? WorkPositionId { get; set; }
        public bool? Active { get; set; } = true;
    }
}
