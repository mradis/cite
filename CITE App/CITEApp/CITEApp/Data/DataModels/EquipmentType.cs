﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Models
{
    [Table("EquipmentType")]
    public class EquipmentType
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; } = true;
    }
}
