﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Models
{
    [Table("WorkPosition")]
    public class WorkPosition
    {
        [Key]
        public Guid Id { get; set; }
        public Guid BranchId { get; set; }
        public Guid? EmployeeId { get; set; }
        public bool Active { get; set; } = true;
        public string Description { get; set; }

        //navigation properties
        public Branch Branch { get; set; }
        public Employee Employee { get; set; }

    }
}
