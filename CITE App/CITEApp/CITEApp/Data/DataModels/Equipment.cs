﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Models
{
    [Table("Equipment")]
    public class Equipment
    {
        [Key]
        public Guid Id { get; set; }
        public string Description { get; set; }
        public Guid TypeId { get; set; }
        public Guid? WorkPositionId { get; set; }
        public bool Active { get; set; } = true;

        //Navigation properties
        public WorkPosition Position { get; set; }
        public EquipmentType EquipmentType { get; set; }
    }
}
