﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Models
{
    [Table("EmployeeWorkingPositionHistory")]
    public class EmployeeWorkingPositionHistory
    {
        [Key]
        public Guid Id { get; set; }
        public Guid WorkPositionId { get; set; }
        public Guid EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? StopDate { get; set; }
        public bool Active { get; set; }

        // Navigation properties
        public Employee Employee { get; set; }
        public WorkPosition Position { get; set; }
    }
}
