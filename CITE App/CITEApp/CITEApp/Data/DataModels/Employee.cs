﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CITEApp.Models
{
    [Table("Employee")]
    public class Employee
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        [Column(TypeName = "decimal(18, 4)")]
        public decimal Salary { get; set; } = 0;
        public DateTime DateStarted { get; set; }
        public DateTime? DateStopped { get; set; }
                                           // public Guid BranchId { get; set; }
        public bool Active { get; set; } = true;

        //navigation property
     //   public Branch Branch { get; set; }
    }
}
