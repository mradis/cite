﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CITEApp._Services.Interfaces;
using CITEApp.Data.Interfaces;
using CITEApp.Dtos;
using CITEApp.Dtos.EmployeeWorkPositionHistoryDtos;
using CITEApp.Helpers;
using CITEApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CITEApp.Controllers
{
    [Route("api/WorkPositionHistory")]
    [ApiController]
    public class WorkPositionHistoryController : ControllerBase
    {
        private readonly ILogger<WorkPositionHistoryController> _logger;
        private readonly IWorkPositionHistoryService _workPositionHistoryService;
        private readonly IUtil _util;

        public WorkPositionHistoryController(ILogger<WorkPositionHistoryController> logger ,IWorkPositionHistoryService workPositionHistoryService , IUtil util)
        {
            _logger = logger;
            _workPositionHistoryService = workPositionHistoryService;
            _util = util;
        }
        // GET: api/WorkPositionHistory
        [HttpGet]
        public async  Task<IEnumerable<GetEmployeeWorkPositionHistoryDTO>> GetAllActiveWorkPositionHistories()
        {
            _logger.LogDebug("Action GetAllWorkPositionHistories Invoked");
            return await _workPositionHistoryService.GetAllHistories(true);
        }

        [HttpGet("/InActive")]
        public async Task<IEnumerable<GetEmployeeWorkPositionHistoryDTO>> GetAllInActiveWorkPositionHistories()
        {
            _logger.LogDebug("Action GetAllWorkPositionHistories Invoked");
            return await _workPositionHistoryService.GetAllHistories(false);
        }

        // GET: api/WorkPositionHistory/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<EmployeeWorkingPositionHistory> GetHistory(Guid id)
        {
            _logger.LogDebug($"Action GetHistory invoked with id: {id}");
            return await _workPositionHistoryService.GetHistory(id, false);
        }

        // POST: api/WorkPositionHistory
        [HttpPost]
        public async Task<EmployeeWorkingPositionHistory> CreateHistory(CreateEmployeeWorkingHistoryVM historyDTO)
        {
            _logger.LogDebug($"Action CreateHistory Invoked with data: {_util.DisplayObjectInfo(historyDTO)}");
            return await _workPositionHistoryService.CreateHistory(historyDTO); ;
        }

        // PUT: api/WorkPositionHistory/5
        [HttpPut]
        public async Task<EmployeeWorkingPositionHistory> UpdateBranchHistory(UpdateEmployeeWorkingPositionHistoryVM historyDTO)
        {
            _logger.LogDebug($"Action UpdateBranch Invoked with data: {_util.DisplayObjectInfo(historyDTO)}");
            return await _workPositionHistoryService.UpdateBranchHistory(historyDTO);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task DeleteHistory(Guid id)
        {
            _logger.LogDebug($"Action DeleteHistory Invoked with id: {id}");
            await _workPositionHistoryService.DeleteHistory(id);
        }
    }
}
