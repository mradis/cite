﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CITEApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ILogger _logger;

        public ValuesController(ILogger<ValuesController> logger)
        {
            _logger = logger;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {        // Log level hierarchy: 
                 // Trace, Debug, Information, Warning, Error, Critical
            _logger.LogTrace("IndexModel.OnGet entered (trace)");
            _logger.LogDebug("IndexModel.OnGet entered (debug)");
            _logger.LogInformation("IndexModel.OnGet entered (info)");
            _logger.LogWarning("IndexModel.OnGet entered (warn)");
            _logger.LogError("IndexModel.OnGet entered (error)");
            _logger.LogCritical("IndexModel.OnGet entered (crit)");
            //_logger.LogInformation("Logging test LogInformation");
            //_logger.LogDebug("Logging test LogDebug");
            //_logger.LogWarning("Logging test LogWarning");
            //_logger.LogCritical("Logging test LogCritical");
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
