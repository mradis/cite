﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CITEApp._Services.Interfaces;
using CITEApp.Data.Interfaces;
using CITEApp.Dtos;
using CITEApp.Dtos.EquipmentDtos;
using CITEApp.Helpers;
using CITEApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CITEApp.Controllers
{
    [Route("api/Equipment")]
    [ApiController]
    public class EquipmentController : ControllerBase
    {
        private readonly ILogger<EquipmentController> _logger;
        private readonly IEquipmentService _equipmentService;
        private readonly IUtil _util;

        public EquipmentController(ILogger<EquipmentController> logger , IEquipmentService equipmentService , IUtil util)
        {
            _logger = logger;
            _equipmentService = equipmentService;
            _util = util;
        }


        // GET: api/Equipment
        [HttpGet]
        public async Task<IEnumerable<GetEquipmentDTO>> GetAllActiveEquipments()
        {
            _logger.LogDebug("Action GetAllEquipments Invoked");
            return await _equipmentService.GetAllEquipments(true);
        }

        [HttpGet("InActive")]
        public async Task<IEnumerable<GetEquipmentDTO>> GetAllInActiveEquipments()
        {
            _logger.LogDebug("Action GetAllEquipments Invoked");
            return await _equipmentService.GetAllEquipments(false);
        }

        // GET: api/Equipment/5
        [HttpGet("{id}")]
        public async Task<Equipment> GetEquipment(Guid id)
        {
            _logger.LogDebug($"Action GetEquipment Invoked with id: {id}");
            return await _equipmentService.GetEquipment(id, false);
        }

        // POST: api/Equipment
        [HttpPost]
        public async Task<Equipment> CreateEquipment(CreateEquipmentVM createEquipmentDTO)
        {
            _logger.LogDebug($"Action CreateBranch Invoked with data: {_util.DisplayObjectInfo(createEquipmentDTO)}");
            return await _equipmentService.CreateEquipment(createEquipmentDTO);
        }

        // PUT: api/Equipment/5
        [HttpPut]
        public async Task<Equipment> UpdateEquipment(UpdateEquipmentVM updateEquipmentDTO)
        {
            _logger.LogDebug($"Action UpdateEquipment Invoked with data: {_util.DisplayObjectInfo(updateEquipmentDTO)}");
            return await _equipmentService.UpdateEquipment(updateEquipmentDTO);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task DeleteEquipment(Guid id)
        {
            _logger.LogDebug($"Action DeleteEquipment Invoked with data: {id}");
            await _equipmentService.DeleteEquipment(id);
        }
    }
}
