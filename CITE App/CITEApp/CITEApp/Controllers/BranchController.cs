﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CITEApp._Services.Interfaces;
using CITEApp.Data;
using CITEApp.Data.Interfaces;
using CITEApp.Dtos.BranchDtos;
using CITEApp.Helpers;
using CITEApp.Helpers.CustomExceptions;
using CITEApp.Helpers.CustomFilters;
using CITEApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace CITEApp.Controllers
{
    [Route("api/branch")]
    [ApiController]
    public class BranchController : ControllerBase
    {
        private readonly IStringLocalizer<BranchController> _localizer;
        private readonly ILogger<BranchController> _logger;
        private readonly IBranchService _branchService;
        private readonly IUtil _util;

        public BranchController(IStringLocalizer<BranchController> localizer, ILogger<BranchController> logger, IBranchService branchService , IUtil util)
        {
            _localizer = localizer;
            _logger = logger;
            _branchService = branchService;
            _util = util;
        }

        [HttpGet]
        public async Task<IEnumerable<GetBranchDTO>> GetAllActiveBranches()
        {
            _logger.LogDebug("Action GetAllBranches Invoked");
            return await _branchService.GetAllBranches(true);
        }

        [HttpGet("InActive")]
        public async Task<IEnumerable<GetBranchDTO>> GetAllInActiveBranches()
        {
            _logger.LogDebug("Action GetAllInActiveBranches Invoked");
            return await _branchService.GetAllBranches(false);
        }
        
        [HttpGet("{id}")]
        public async Task<Branch> GetBranch(Guid id)
        {
            _logger.LogDebug($"Action GetBranch invoked with id: {id}");
            return await _branchService.GetBranch(id,false);
        }

        [HttpPost]
        public async Task<Branch> CreateBranch(CreateBranchVM createBranchDTO)
        {
            _logger.LogDebug($"Action CreateBranch Invoked with data: {_util.DisplayObjectInfo(createBranchDTO)}");
            return await _branchService.CreateBranch(createBranchDTO);
        }

        [HttpPut]
        public async Task<Branch> UpdateBranch(UpdateBranchVM branch)
        {
            _logger.LogDebug($"Action UpdateBranch Invoked with data: {_util.DisplayObjectInfo(branch)}");
            return await _branchService.UpdateBranch(branch);
        }

        [ServiceFilter(typeof(TransactionActionFilter))]
        [HttpDelete("{id}")]
        public async Task DeleteBranch(Guid id)
        {
            _logger.LogDebug($"Action DeleteBranch Invoked with data: {id}");
            await _branchService.DeleteBranch(id);
        }
    }
}