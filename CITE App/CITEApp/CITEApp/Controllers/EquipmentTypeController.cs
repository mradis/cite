﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CITEApp._Services.Interfaces;
using CITEApp.Data.Interfaces;
using CITEApp.Dtos.EquipmentTypeDtos;
using CITEApp.Helpers;
using CITEApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CITEApp.Controllers
{
    [Route("api/EquipmentType")]
    [ApiController]
    public class EquipmentTypeController : ControllerBase
    {
        private readonly ILogger<EquipmentTypeController> _logger;
        private readonly IEquipmentTypeService _equipmentTypeService;
        private readonly IUtil _util;

        public EquipmentTypeController(ILogger<EquipmentTypeController> logger, IEquipmentTypeService equipmentTypeService, IUtil util)
        {
            _logger = logger;
            _equipmentTypeService = equipmentTypeService;
            _util = util;
        }
        // GET: api/EquipmentType
        [HttpGet]
        public async Task<IEnumerable<GetEquipmentTypeVM>> GetAllActiveTypes()
        {
            _logger.LogDebug($"Action GetAllTypes Invoked");
            return await _equipmentTypeService.GetTypes(true);
        }

        [HttpGet("/InActive")]
        public async Task<IEnumerable<GetEquipmentTypeVM>> GetAllInActiveTypes()
        {
            _logger.LogDebug($"Action GetAllTypes Invoked");
            return await _equipmentTypeService.GetTypes(false);
        }

        // GET: api/EquipmentType/5
        [HttpGet("{id}")]
        public async Task<EquipmentType> GetType(Guid id)
        {
            _logger.LogDebug($"Action GetType Invoked with id: {id}");
            return await _equipmentTypeService.GetEquipmentType(id, false);
        }

        // POST: api/EquipmentType
        [HttpPost]
        public async Task<EquipmentType> CreateType([FromBody] CreateEquipmentTypeDTO equipmentType)
        {
            _logger.LogDebug($"Action CreateType Invoked with data: Name={_util.DisplayObjectInfo(equipmentType)}");
            return await _equipmentTypeService.CreateEquipmentType(equipmentType);
        }

        // PUT: api/EquipmentType/5
        [HttpPut("{id}")]
        public async Task<EquipmentType> UpdateType(UpdateEquipmentTypeDTO equipmentType)
        {
            _logger.LogDebug($"Action UpdateType Invoked with data: {_util.DisplayObjectInfo(equipmentType)}");
            return await _equipmentTypeService.UpdateEquipmentType(equipmentType);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task DeleteType(Guid id)
        {
            _logger.LogDebug($"Action DeleteType Invoked with id: {id}");
            await _equipmentTypeService.DeleteEquipmentType(id);
        }
    }
}
