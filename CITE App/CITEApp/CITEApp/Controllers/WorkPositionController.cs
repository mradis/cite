﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CITEApp._Services.Interfaces;
using CITEApp.Data.Interfaces;
using CITEApp.Dtos;
using CITEApp.Dtos.WorkPositionDtos;
using CITEApp.Helpers;
using CITEApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CITEApp.Controllers
{
    [Route("api/WorkPosition")]
    [ApiController]
    public class WorkPositionController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IWorkPositionService _workPositionService;
        private readonly IUtil _util;

        public WorkPositionController(ILogger<WorkPositionController> logger , IWorkPositionService workPositionService , IUtil util)
        {
            _logger = logger;
            _workPositionService = workPositionService;
            _util = util;
        }

        // GET: api/WorkPosition
        [HttpGet]
        public async Task<IEnumerable<GetWorkPositionVM>> GetAllActiveWorkPositions()
        {
            _logger.LogDebug("Action GetAllWorkPositions Invoked");
            return await _workPositionService.GetAllWorkPositions(true);
        }

        [HttpGet("InActive")]
        public async Task<IEnumerable<GetWorkPositionVM>> GetAllInActiveWorkPositions()
        {
            _logger.LogDebug("Action GetAllWorkPositions Invoked");
            return await _workPositionService.GetAllWorkPositions(false);
        }

        // GET: api/WorkPosition/5
        [HttpGet("{id}")]
        public async Task<WorkPosition> GetWorkPosition(Guid id)
        {
            _logger.LogDebug($"Action GetWorkPosition Invoked with id: {id}");
            return await _workPositionService.GetWorkPosition(id, false);
        }

        // POST: api/WorkPosition
        [HttpPost]
        public async Task<WorkPosition> CreateWorkPosition(CreateWorkPositionDTO  workPositionDTO)
        {
            _logger.LogDebug($"Action CreateWorkPosition Invoked with data: {_util.DisplayObjectInfo(workPositionDTO)}");
            return await _workPositionService.CreateWorkPosition(workPositionDTO);
        }

        // PUT: api/WorkPosition/5
        [HttpPut("{id}")]
        public async Task<WorkPosition> UpdateWorkPosition([FromBody]UpdateWorkPositionDTO workPositionDTO)
        {
            _logger.LogDebug($"Action UpdateWorkPosition Invoked with data: {_util.DisplayObjectInfo(workPositionDTO)}");
            return await _workPositionService.UpdateWorkPosition(workPositionDTO);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task DeleteWorkPosition(Guid id)
        {
            _logger.LogDebug($"Action DeleteWorkPosition Invoked with id: {id}");
            await _workPositionService.DeleteWorkPosition(id);
        }

        // PUT: api/WorkPosition/5/AddEmployee/10
        [HttpPut("{branchId}/AddEmployee/{employeeId}")]
        public async Task AddEmployeeWorkPosition(Guid positionId, Guid employeeId)
        {
            _logger.LogDebug($"Action AddEmployeeBranch invoked with employee id: {employeeId} and branch id: {positionId}");
            await _workPositionService.AddEmployeeWorkPosition(positionId,employeeId);
        }

        [HttpPut("{branchId}/RemoveEmployee/{employeeId}")]
        public async Task RemoveEmployeeWorkPosition(Guid positionId, Guid employeeId)
        {
            _logger.LogDebug($"Action AddEmployeeBranch invoked with employee id: {employeeId} and branch id: {positionId}");
            await _workPositionService.RemoveEmployeeWorkPosition(positionId, employeeId);
        }
    }
}
