﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CITEApp._Services.Interfaces;
using CITEApp.Data.Interfaces;
using CITEApp.Dtos;
using CITEApp.Dtos.EmployeeDtos;
using CITEApp.Helpers;
using CITEApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CITEApp.Controllers
{
    [Route("api/Employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly IEmployeeService _employeeService;
        private readonly IUtil _util;

        public EmployeeController(ILogger<EmployeeController> logger, IEmployeeService employeeService, IUtil util)
        {
            _logger = logger;
            _employeeService = employeeService;
            _util = util;
        }
        // GET: api/Employee
        [HttpGet]
        public async  Task<IEnumerable<GetEmployeeVM>> GetAllActiveEmployees()
        {
            _logger.LogDebug("Action GetAllEmployees Invoked");
            return await _employeeService.GetAllEmployees(true);
        }

        [HttpGet("/InActive")]
        public async Task<IEnumerable<GetEmployeeVM>> GetAllInActiveEmployees()
        {
            _logger.LogDebug("Action GetAllEmployees Invoked");
            return await _employeeService.GetAllEmployees(false);
        }

        // GET: api/Employee/5
        [HttpGet("{id}")]
        public async Task<Employee> GetEmployee(Guid id)
        {
            _logger.LogDebug($"Action GetEmployee Invoked with id: {id}");
            return await _employeeService.GetEmployee(id, false);
        }

        // POST: api/Employee
        [HttpPost]
        public async Task<Employee> CreateEmployee([FromBody] CreateEmployeeDTO employee)
        {
            _logger.LogDebug($"Action CreateEmployee Invoked with data: {_util.DisplayObjectInfo(employee)}");
            return await _employeeService.CreateEmployee(employee);
        }

        // PUT: api/Employee/5
        [HttpPut("{id}")]
        public async Task<Employee> UpdateEmployee([FromBody] UpdateEmployeeDTO employee)
        {
            _logger.LogDebug($"Action UpdateEmployee Invoked with data: {_util.DisplayObjectInfo(employee)}");
            return await _employeeService.UpdateEmployee(employee);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task DeleteEmployee(Guid id)
        {
            _logger.LogDebug($"Action DeleteEmployee Invoked with id: {id}");
            await _employeeService.DeleteEmployee(id);
        }
    }
}
