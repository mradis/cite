import { EquipmentType } from '../equipment-type/equipment-type';
import { WorkPosition } from '../work-position/work-position';
import { FormGroup, FormControl, Validators } from '@angular/forms';

export interface Equipment {
    id?: string;
    description: string;
    typeOfEquipment: EquipmentType;
    workPosition?: WorkPosition;
}

export class EquipmentViewModel implements Equipment {
    id?: string;
    description: string;
    typeOfEquipment: EquipmentType;
    workPosition?: WorkPosition;

    constructor(equipment: Equipment) {
        this.id = equipment.id;
        this.description = equipment.description;
        this.typeOfEquipment = equipment.typeOfEquipment;
        this.workPosition = equipment.workPosition;
    }
    toFormGroup(): FormGroup {
        return new FormGroup({
            id: new FormControl(this.id),
            description: new FormControl(this.description, Validators.required),
            typeOfEquipment: new FormControl(this.typeOfEquipment, Validators.required),
            workPosition: new FormControl(this.workPosition)
        });
    }
}
