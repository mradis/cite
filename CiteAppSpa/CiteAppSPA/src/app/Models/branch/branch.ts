export interface Branch {
    id?: string;
    name: string;
    address: string;
    created: Date;
    isActive: boolean;
}

