export interface BranchPersist {
    name: string;
    address: string;
}
