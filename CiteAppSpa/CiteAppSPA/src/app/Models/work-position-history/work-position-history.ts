import { WorkPosition } from '../work-position/work-position';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Employee } from '../employee/employee';

export interface WorkPositionHistory {
    id?: string;
    workPositionId: WorkPosition;
    employeeId: Employee;
    startDate: Date;
    stopDate?: Date;
}


export class WorkPositionHistoryViewModel implements WorkPositionHistory {
    id?: string;
    workPositionId: WorkPosition;
    employeeId: Employee;
    startDate: Date;
    stopDate?: Date;

    constructor(workPositionHistory: WorkPositionHistory) {
        this.id = workPositionHistory.id;
        this.workPositionId = workPositionHistory.workPositionId;
        this.employeeId = workPositionHistory.employeeId;
        this.startDate = workPositionHistory.startDate;
        this.stopDate = workPositionHistory.stopDate;

    }
    toFormGroup(): FormGroup {
        return new FormGroup({
            id: new FormControl(this.id),
            workPositionId: new FormControl(this.workPositionId, Validators.required),
            employeeId: new FormControl(this.employeeId, Validators.required),
            startDate: new FormControl(this.startDate, Validators.required),
            stopDate: new FormControl(this.stopDate),
        });
    }
}
