import { FormGroup, FormControl, Validators } from '@angular/forms';

export interface Employee {
    id?: string;
    name: string;
    surname: string;
    salary?: number;
    dateStarted: Date;
    dateStopped?: Date;
}

export class EmployeeViewModel implements Employee {
    id?: string;    name: string;
    surname: string;
    salary?: number;
    dateStarted: Date;
    dateStopped?: Date;


    constructor(employee: Employee) {
        this.id = employee.id;
        this.surname = employee.surname;
        this.salary = employee.salary;
        this.dateStarted = employee.dateStarted;
        this.dateStopped = employee.dateStopped;

    }
    toFormGroup(): FormGroup {
        return new FormGroup({
            id: new FormControl(this.id),
            surname: new FormControl(this.surname, Validators.required),
            salary: new FormControl(this.salary),
            dateStarted: new FormControl(this.dateStarted, Validators.required),
            dateStopped: new FormControl(this.dateStopped)
        });
    }


}
