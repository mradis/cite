import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Employee } from '../employee/employee';
import { Branch } from '../Branch/Branch';

export interface WorkPosition {
    id?: string;
    branchId: Branch;
    employeeId?: Employee;
    description: string;
}


export class WorkPositionViewModel implements WorkPosition {
    id?: string; branchId: Branch;
    employeeId?: Employee;
    description: string;

    constructor(workPosition: WorkPosition) {
        this.id = workPosition.id;
        this.employeeId = workPosition.employeeId;
        this.description = workPosition.description;
    }
    toFormGroup(): FormGroup {
        return new FormGroup({
            id: new FormControl(this.id),
            employeeId: new FormControl(this.employeeId),
            description: new FormControl(this.description, Validators.required)
        });
    }
}
