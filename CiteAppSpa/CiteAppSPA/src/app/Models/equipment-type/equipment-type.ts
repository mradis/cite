import { FormGroup, FormControl, Validators } from '@angular/forms';

export interface EquipmentType {
    id?: string;
    category: string;
}

export class EquipmentTypeViewModel implements EquipmentType {
    id?: string;
    category: string;

    constructor(equipmentType?: EquipmentType) {
        if (equipmentType) {
            this.id = equipmentType.id;
            this.category = equipmentType.category;
        }
    }

    toFormGroup(): FormGroup {
        return new FormGroup({
            id : new FormControl(this.id),
            category : new FormControl(this.category, Validators.required)
        });
    }
}
