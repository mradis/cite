import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Branch } from '../Models/Branch/Branch';

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  basePort = '5000';
  baseUrl = 'http://localhost:' + this.basePort + '/api/branch';

  constructor(private http: HttpClient) { }

  getAllActiveBranches() {
    return this.http.get<Branch[]>(this.baseUrl);
  }

  createBranch(branch: Branch) {
    return this.http.post(this.baseUrl, branch);
  }

  updateBranch(branch: Branch) {
    return this.http.put( this.baseUrl + '/', branch);
  }

  deleteBranch(id: string) {
    return this.http.delete(this.baseUrl + '/' + id);
  }
}
