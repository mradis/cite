import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EquipmentType } from '../Models/equipment-type/equipment-type';

@Injectable({
  providedIn: 'root'
})
export class EquipmentTypeService {
  basePort = '5000';
  baseUrl = 'http://localhost:' + this.basePort + '/api/EquipmentType';


constructor(private http: HttpClient) { }

getAllActiveTypes() {
  return this.http.get<EquipmentType[]>(this.baseUrl);
}

createType(type: EquipmentType) {
  return this.http.post(this.baseUrl, type );
}

updateType(type: EquipmentType) {
  return this.http.put( this.baseUrl + '/', type);
}

deleteType(id: string) {
  return this.http.delete(this.baseUrl + '/' + id);
}
}
