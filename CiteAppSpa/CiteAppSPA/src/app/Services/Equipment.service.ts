import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Equipment } from '../Models/equipment/equipment';

@Injectable({
  providedIn: 'root'
})
export class EquipmentService {
  basePort = '5000';
  baseUrl = 'http://localhost:' + this.basePort + '/api/equipment';

  constructor(private http: HttpClient) { }

  getAllActiveEquipments() {
    return this.http.get<Equipment[]>(this.baseUrl);
  }

  createEquipment(equipment: Equipment) {
    return this.http.post(this.baseUrl, equipment);
  }

  updateEquipment(equipment: Equipment) {
    return this.http.put( this.baseUrl + '/', equipment);
  }

  deleteEquipment(id: string) {
    return this.http.delete(this.baseUrl + '/' + id);
  }
}
