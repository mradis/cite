import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AlertMeService {

constructor(private snackBar: MatSnackBar) { }

successSnackBar(message: string, action: string, durationInSecs?: number) {
  if (!durationInSecs) {
    durationInSecs = 2;
  }
  this.snackBar.open(message, action, {
    duration: 1000 * durationInSecs,
    panelClass: ['style-success']
  });
}

warningSnackBar(message: string, action: string, durationInSecs?: number) {
  if (!durationInSecs) {
    durationInSecs = 2;
  }
  this.snackBar.open(message, action, {
    duration: 1000 * durationInSecs,
    panelClass: ['style-warning']
  });
}

errorSnackBar(message: string, action: string, durationInSecs?: number) {
  if (!durationInSecs) {
    durationInSecs = 2;
  }
  this.snackBar.open(message, action, {
    duration: 1000 * durationInSecs,
    panelClass: ['style-error']
  });
}

}
