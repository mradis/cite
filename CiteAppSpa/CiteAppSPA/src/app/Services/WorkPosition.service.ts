import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WorkPosition } from '../Models/work-position/work-position';

@Injectable({
  providedIn: 'root'
})
export class WorkPositionService {
  basePort = '5000';
  baseUrl = 'http://localhost:' + this.basePort + '/api/workPosition';

  constructor(private http: HttpClient) { }

  getAllActiveWorkPositions() {
    return this.http.get<WorkPosition[]>(this.baseUrl);
  }

  createWorkPosition(workPosition: WorkPosition) {
    return this.http.post(this.baseUrl, workPosition );
  }

  updateWorkPosition(workPosition: WorkPosition) {
    return this.http.put( this.baseUrl + '/', workPosition);
  }

  deleteWorkPosition(id: string) {
    return this.http.delete(this.baseUrl + '/' + id);
  }
}
