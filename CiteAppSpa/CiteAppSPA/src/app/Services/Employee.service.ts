import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../Models/employee/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  basePort = '5000';
  baseUrl = 'http://localhost:' + this.basePort + '/api/employee';

  constructor(private http: HttpClient) { }

  getAllActiveEmployees() {
    return this.http.get<Employee[]>(this.baseUrl);
  }

  createEmployee(employee: Employee) {
    return this.http.post(this.baseUrl, employee);
  }

  updateEmployee(employee: Employee) {
    return this.http.put( this.baseUrl + '/', employee);
  }

  deleteEmployee(id: string) {
    return this.http.delete(this.baseUrl + '/' + id);
  }
}
