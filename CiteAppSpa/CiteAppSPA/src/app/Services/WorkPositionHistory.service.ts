import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WorkPositionHistory } from '../Models/work-position-history/work-position-history';

@Injectable({
  providedIn: 'root'
})
export class WorkPositionHistoryService {
  basePort = '5000';
  baseUrl: string = 'http://localhost:' + this.basePort + '/api/WorkPositionHistory';

  constructor(private http: HttpClient) { }

  getAllActiveWorkPositionsHistories() {
    return this.http.get<WorkPositionHistory[]>(this.baseUrl);
  }

  createWorkPositionHistory(history: WorkPositionHistory) {
    return this.http.post(this.baseUrl, history );
  }

  updateWorkPositionHistory(history: WorkPositionHistory) {
    return this.http.put( this.baseUrl + '/', history );
  }

  deleteWorkPositionHistory(id: string) {
    return this.http.delete(this.baseUrl + '/' + id);
  }
}
