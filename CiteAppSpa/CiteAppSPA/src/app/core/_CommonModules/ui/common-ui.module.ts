import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../material-module/material.module';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        MaterialModule
    ],
    exports: [
        CommonModule,
        MaterialModule
    ]
})

export class CommonUiModule { }
