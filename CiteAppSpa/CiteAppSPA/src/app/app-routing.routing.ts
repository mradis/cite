import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'Branches', pathMatch: 'full' },
  { path: 'Branches', loadChildren: './_UI/branch/Branch.module#BranchesModule' },
  { path: 'Employees', loadChildren: './_UI/employee/Employee.module#EmployeesModule' },
  { path: 'Equipment', loadChildren: './_UI/equipment/Equipment.module#EquipmentModule' }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
