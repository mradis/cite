import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { EmployeesComponent } from './editor/employee/Employees.component';

const routes: Routes = [
  {path: "", redirectTo:"Employees", pathMatch:"full"  },
  {path: "Employees", component: EmployeesComponent  }
];

@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})

export class EmployeesRoutingModule {};