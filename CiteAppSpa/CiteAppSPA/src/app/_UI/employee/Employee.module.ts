import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { EmployeesRoutingModule } from './Employees-routing.routing';
import { EmployeesComponent } from './editor/employee/Employees.component';
import { MaterialModule } from 'src/app/core/_CommonModules/material-module/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    EmployeesRoutingModule
  ],
  declarations: [
  EmployeesComponent
  ]
})
export class EmployeesModule { }
