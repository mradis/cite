import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EquipmentComponent } from './editor/Equipment/Equipment.component';
import { EquipmentRoutingModule } from './Equipment.routing';
import { EquipmentTypeComponent } from './editor/equipment-type/EquipmentType.component';
import { MaterialModule } from 'src/app/core/_CommonModules/material-module/material.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    EquipmentRoutingModule
  ],
  declarations: [
  EquipmentComponent,
  EquipmentTypeComponent
  ]
})
export class EquipmentModule { }
