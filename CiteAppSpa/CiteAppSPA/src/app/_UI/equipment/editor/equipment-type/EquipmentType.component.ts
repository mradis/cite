import { FormGroup } from '@angular/forms';
import { EquipmentType } from 'src/app/Models/equipment-type/equipment-type';
import { EquipmentTypeService } from 'src/app/Services/EquipmentType.service';
import { OnInit, Component } from '@angular/core';

@Component({
  selector: 'app-equipmenttype',
  templateUrl: './EquipmentType.component.html',
  styleUrls: ['./EquipmentType.component.css']
})
export class EquipmentTypeComponent implements OnInit {
  types: EquipmentType[] = [];
  constructor(private typeService: EquipmentTypeService) { }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.typeService.getAllActiveTypes().subscribe(res => {
        this.types = res;
        console.log(this.types);
      }, error => console.log(error.error)
    );
  }

  createType(form: FormGroup) {
    const typeToCreate: EquipmentType = form.value;
    this.typeService.createType(typeToCreate).subscribe(
      res => {
        console.log('Type created successfully');
      }, error => console.log(error.error)
    );
  }

  updateType(form: FormGroup) {
    const typeToUpdate: EquipmentType = form.value;
    this.typeService.updateType(typeToUpdate).subscribe(res => {
    }, error => console.log(error.error)
    );
  }

  deleteType(form: FormGroup) {
    this.typeService.deleteType(form.get('id').value).subscribe(res => {
    }, error => console.log(error.error)
    );
  }

}
