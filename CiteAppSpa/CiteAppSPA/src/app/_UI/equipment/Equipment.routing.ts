import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { EquipmentComponent } from './editor/Equipment/Equipment.component';

const routes: Routes = [
  {path: "", redirectTo:"Equipment", pathMatch:"full"  },
  {path: "Equipment", component: EquipmentComponent  }
];

@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})

export class EquipmentRoutingModule {};