import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, Form, FormsModule } from '@angular/forms';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { MatSnackBar, MatSnackBarConfig, MatExpansionPanel, PageEvent } from '@angular/material';
import { AlertMeService } from 'src/app/Services/alert.service';
import { Branch } from 'src/app/Models/branch/branch';
import { BranchService } from 'src/app/Services/Branch.service';

@Component({
  selector: 'app-branches',
  templateUrl: './Branches.component.html',
  styleUrls: ['./Branches.component.css']
})
export class BranchesComponent implements OnInit {
  panelOpenState = false;
  branches: Branch[];
  branch: object;
  forms: FormGroup[] = [];
  branchFormGroup: FormGroup;
  durationInSeconds = 5;
  @ViewChild('createBranch')
  private elCreate: MatExpansionPanel;
  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;

  constructor(private branchService: BranchService, private alert: AlertMeService) { }

  ngOnInit() {
  }

  create(form: FormGroup) {
    const branchToCreate: Branch = form.value;
    if (form.status === 'VALID') {
      this.branchService.createBranch(branchToCreate).subscribe(res => {
        this.alert.successSnackBar('Branch created', '');
        this.forms = [];
        this.refresh();
        this.elCreate.close();
      }, error => {
        this.alert.errorSnackBar(error.error, '');
      });
    }
  }
  // TODO:Check Form Status
  update(form: FormGroup) {
    const branchToUpdate: Branch = form.value;
    this.branchService.updateBranch(branchToUpdate).subscribe(res => {
      this.forms = [];
      this.refresh();
    }, error => console.log(error.error));
  }

  deleteBranch(form: FormGroup) {
    this.branchService.deleteBranch(form.get('id').value).subscribe(res => {
      this.forms = [];
      this.refresh();
    }, error => {
      // TODO: Use Dialog Box
      this.alert.errorSnackBar(error.error, '', 5);
      this.forms = [];
      this.refresh();
    });
  }

  refresh = () => {
    this.branchService.getAllActiveBranches().subscribe(res => {
      this.branches = res;
      this.createFormGroups(this.branches);
    }, error => console.log('An error has occurred ' + error.error));
  }

  createFormGroups(branches) {
    branches.forEach(branch => {
    });
  }

  getServerData(event?: PageEvent) {
    console.log(event);

    this.alert.successSnackBar('event emmited', '');
  }

  getErrorMessage(createBranch: FormGroup, formControlName: string) {

    return createBranch.get(formControlName).hasError('required') ? 'You must enter a value' + '\n' + 'lathos test' : '';
  }

  hasErrorMessage(createBranch: FormGroup, formControlName: string) {

    return createBranch.get(formControlName).hasError('required') ? 'You must enter a value' + '\n' + 'lathos test' : '';
  }

}
