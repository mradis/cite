import { Branch } from 'src/app/Models/branch/branch';

export class BranchEditorModel  implements Branch {
    isActive: boolean;
    id?: string;
    name: string;
    address: string;
    created: Date;

constructor() { }

    public fromModel(item: Branch): BranchEditorModel {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            this.address = item.address;
            this.created = item.created;
        }
        return this;
    }
}
