import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BranchRoutingModule } from './branch-routing.routing';
import { BranchesComponent } from './editor/Branches.component';
import { MaterialModule } from 'src/app/core/_CommonModules/material-module/material.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    BranchRoutingModule,

  ],
  declarations: [
    BranchesComponent,
  ]
})
export class BranchesModule { }
