import { Component, OnInit } from '@angular/core';
import { BaseComponent } from './core/base/base.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends BaseComponent implements OnInit {


  constructor(
    public translate: TranslateService,
  ) {
    super();
    this.initializeServices();
    console.log('eimai');

  }

  ngOnInit(): void {

  }


  initializeServices() {
        // this language will be used as a fallback when a translation isn't found in the current language
        this.translate.setDefaultLang('el');
        // the lang to use, if the lang isn't available, it will use the current loader to get them
        this.translate.addLangs(['en', 'el']);

        const browserLang = this.translate.getBrowserLang();
        this. translate.use(browserLang.match(/en|el/) ? browserLang : 'en');
  }


}
