import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.routing';
import { NavComponent } from './_UI/_nav/nav.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {CommonUiModule} from './core/_CommonModules/ui/common-ui.module';
import { AppComponent } from './app.component';

export function createTranslateLoader(http: HttpClient) {
   return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
   declarations: [
      AppComponent,
      NavComponent
   ],
   imports: [
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      BrowserModule,
      BrowserAnimationsModule,
      AppRoutingModule,
      CommonUiModule,
      TranslateModule.forRoot({
         loader: {
             provide: TranslateLoader,
             useFactory: createTranslateLoader,
             deps: [HttpClient]
         }
     })
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
